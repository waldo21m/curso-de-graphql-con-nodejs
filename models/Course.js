const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema(
  {
    title: String,
    views: Number,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  }
);

const Course = mongoose.model('Course', courseSchema);

module.exports = Course;
