# Curso de GraphQL con NodeJS

## Lecciones:

* [Introducción](#module01)
    * [Lección #1 - Presentación del Curso](#lesson01)
    * [Lección #2 - ¿Qué aprenderé en este curso?](#lesson02)
    * [Lección #3 - ¿Qué es un servicio web?](#lesson03)
    * [Lección #4 - ¿Qué es GraphQL?](#lesson04)
    * [Lección #5 - Servidor HTTP con Express](#lesson05)
    * [Lección #6 - Definir el schema](#lesson06)
    * [Lección #7 - Hola mundo con GraphQL](#lesson07)
    * [Lección #8 - Tipos objeto en el schema](#lesson08)
* [Fundamentos de GraphQL](#module02)
    * [Lección #9 - El schema](#lesson09)
    * [Lección #10 - Sistema de tipado en GraphQL](#lesson10)
    * [Lección #11 - Los tipos Query y Mutation](#lesson11)
* [Mi primer servicio web con GraphQL](#module03)
    * [Lección #12 - Preparando el proyecto](#lesson12)
    * [Lección #13 - GraphQL Schema Language](#lesson13)
    * [Lección #14 - Mocking de datos](#lesson14)
    * [Lección #15 - Preparando las consultas](#lesson15)
    * [Lección #16 - GraphiQL](#lesson16)
    * [Lección #17 - Consultar recurso individual](#lesson17)
    * [Lección #18 - Variables de consulta](#lesson18)
    * [Lección #19 - Mutaciones](#lesson19)
    * [Lección #20 - Actualizar registros](#lesson20)
    * [Lección #21 - Input types](#lesson21)
    * [Lección #22 - Eliminar registros](#lesson22)
    * [Lección #23 - Paginación](#lesson23)
* [Apollo GraphQL](#module04)
    * [Lección #24 - ¿Qué es Apollo GraphQL?](#lesson24)
    * [Lección #25 - Servidor con ApolloServer](#lesson25)
    * [Lección #26 - Definición de tipos](#lesson26)
    * [Lección #27 - Queries](#lesson27)
    * [Lección #28 - Mutaciones](#lesson28)
    * [Lección #29 - Terminando el servidor](#lesson29)
* [Base de datos](#module05)
    * [Lección #30 - Configurando la base de datos](#lesson30)
    * [Lección #31 - Modelo](#lesson31)
    * [Lección #32 - Configurar GraphQL con Express](#lesson32)
    * [Lección #33 - Definición de tipos organizada por archivos](#lesson33)
    * [Lección #34 - Resolvers modularizados](#lesson34)
    * [Lección #35 - Crear registros](#lesson35)
    * [Lección #36 - Consultar registros](#lesson36)
    * [Lección #37 - Actualizar registros](#lesson37)
    * [Lección #38 - Eliminar registros](#lesson38)
    * [Lección #39 - Paginación](#lesson39)
* [Relaciones](#module06)
    * [Lección #40 - Nuevo modelo de usuario](#lesson40)
    * [Lección #41 - Establecer relaciones](#lesson41)
    * [Lección #42 - Relaciones en GraphQL](#lesson42)
    * [Lección #43 - Resolvers para subcampos](#lesson43)
* [Autenticación](#module07)
    * [Lección #44 - Encriptar contraseña](#lesson44)
    * [Lección #45 - Iniciar sesión](#lesson45)
    * [Lección #46 - Generando JSON Web Token](#lesson46)
    * [Lección #47 - Apollo Server v2](#lesson47)
    * [Lección #48 - Contexto GraphQL](#lesson48)
    * [Lección #49 - Cómo proteger mis endpoints](#lesson49)

## <a name="module01"></a> Introducción

### <a name="lesson01"></a> Lección #1 - Presentación del Curso
- - -
En este curso se verá que es GraphQL, fundamentos, creación de servicios, etc.

### <a name="lesson02"></a> Lección #2 - ¿Qué aprenderé en este curso?
- - -
Este curso está enfocado en el desarrollo de servicios con GraphQL aunque también aprenderemos como hacer consultas con lenguaje de consulta de GraphQL.

### <a name="lesson03"></a> Lección #3 - ¿Qué es un servicio web?
- - -
Un servicio web es una forma de comunicación diseñada para máquinas, que establece una serie de normas para la comunicación de información entre dispositivos a través de la red.

Decimos que está diseñado para máquinas porque el propósito de un servicio web es exponer información usando un formato que sea fácil de descifrar por otro programa, esto permite que desarrollemos aplicaciones que consulten esta información y la procesen de una manera simple.

A diferencia de otras interfaces que entregan información, los servicios web específicamente comunican información a través de la web precisamente, utilizando el protocolo HTTP, el mismo que hace que la web funcione, una aplicación hace consultas a otra a través de la red, por lo que nuestro servicio web puede estar en una computadora, que siempre y cuando sea accesible vía internet, pueda entregar información a otros dispositivos sin importar de qué tipo sean éstos, dónde estén o qué lenguaje de programación estén usando para consultar los datos.

Un servicio web es un tipo de API, Application Programming Interface, por lo que en la práctica los conceptos de API y servicio web suelen usarse como si fueran sinónimos. Al igual que otras APIs, un servicio web expone información y operaciones, ocultando los detalles de cómo se procesa esta información o cómo se ejecutan estas operaciones, por ejemplo, si yo, vía un servicio web solicito los cursos de CódigoFacilito, no necesito saber cómo va a obtener esa información, si los consultará de una base de datos, qué base de datos es, cómo están estructurados los datos, si los traerá de otro servicio web, en lo que a mi que necesito la información compete, solo debo de pedir los datos y esperar que se me entreguen, de nuevo, sin conocer los detalles de cómo obtendrá la información.

GraphQL nos permite desarrollar servicios web, con el añadido de que además de exponer los datos y las reglas para consultarlos, además nos provee de un lenguaje de consulta para de manera dinámica consultar información del servicio web.

Además de GraphQL existen otras arquitecturas y estrategias para implementar servicios web, pero la idea es la misma, usar la web y el protocolo HTTP para comunicar datos de un dispositivo a otros.

Esta idea nos permite tener un servicio central de información y distintas aplicaciones consultando y actualizando la información a través del servicio web.

### <a name="lesson04"></a> Lección #4 - ¿Qué es GraphQL?
- - -
GraphQL engloba dos elementos principalmente, por un lado un lenguaje de consulta que le permite a los clientes que consumen un servicio web, especificar qué datos necesitan. Por otro lado, es un entorno de ejecución para responder a estas consultas a través de la especificación de un esquema tipado en el que se enlistan los datos que el servicio web puede entregar y las operaciones para dar respuesta a las solicitudes de los clientes.

Entonces, hay una definición completa de cómo consultar datos, piensa como el lenguaje SQL te permite hacer N cantidad de cosas con los datos de una base de datos, el lenguaje de GraphQL es similar aunque mucho más limitado para un servicio web.

Una consulta con el lenguaje de GraphQL se ve como aparece en el siguiente código:

```sh
{
  courses {
    title
  }
}

```

El lenguaje de consultas de GraphQL es muy expresivo y es parte fundamental de la tecnología, ya que nos permite declarar qué información queremos del servicio web de manera detallada, en el ejemplo anterior podemos asumir que queremos los cursos, PERO, únicamente el título de ellos.

A diferencia de las alternativas a GraphQL donde las respuestas para una consulta están predefinidas y no pueden modificarse por parte del cliente, las consultas de GraphQL son dinámicas, el cliente dice qué quiere y en qué formato lo quiere.

La pregunta es, cómo puede GraphQL entender estas queries y dar una respuesta apropiada.

Aquí es donde el entorno de ejecución entra en juego, usando un lenguaje de backend y las librerías correspondientes de GraphQL, uno puede definir un esquema en donde se especifique qué datos expondremos, cuál es su formato, de qué tipo son los datos que se exponen, y el código que dará respuesta a las consultas.

Un esquema puede verse como ves en pantalla:

```sh
type Course {
   id: ID!
   title: String!
   views: Int
 }
type Query {
   courses(page: Int,limit: Int = 1): [Course]
   course(id : ID!): Course
 }
```

Ahí se especifica por un lado el tipo curso y sus propiedades, además de las operaciones de consulta que se pueden realizar y qué retornan dichas operaciones.

Cuando intentamos hacer una consulta a un servicio web de GraphQL, el cliente descarga el esquema, y en base a la información ahí provista puede saber qué operaciones realizar y qué datos puede consultar, por ejemplo, puede saber que de un curso podrá obtener un id, un título o las vistas.

Además de esta definición de tipos, el esquema se compone de una serie de resolvers, como los llamamos en la terminología de GraphQL, estos resolvers son funciones que dan respuesta a las consultas realizadas. En el caso del esquema actual, se requerirían dos resolver, uno para la consulta courses y otro para course.

Estos resolvers de nuevo, son simplemente funciones que responden a las consultas, quizás trayendo los datos de una base de datos, de un arreglo, de otro servicio web, eso ya no importa, solo tienen que retornar los recursos apropiados.

En base a los datos que retornan los resolvers, GraphQL filtrará la información para únicamente entregar lo que se solicitó en la consulta, de manera que si siguiendo con nuestro ejemplo el resolver de courses retorna el siguiente arreglo:

```sh
[
 { id: "1", title: 'Curso de GraphQL', views: 1000 },
 { id: "2", title: 'Curso de JavaScript', views: 50000 }
]
```

GraphQL filtrará las propiedades y sólo dejará el title para una consulta como la antes vista:

```sh
{
  courses {
    title
  }
}
```

Esto quiere decir que tú no necesitas filtrar ni interpretar la consulta, las herramientas de implementación de GraphQL lo hacen por ti.

Luego de implementar el servidor, recibiríamos una respuesta como esta a la consulta antes realizada:

```sh
{
 "data": {
   "courses": [
     {
       "title": "Curso de GraphQL"
     },
     {
       "title": "Curso de JavaScript"
     }
   ]
 }
}
```

Tenemos los cursos, pero únicamente la propiedad title que solicitamos.

A lo largo del curso irás aprendiendo mucho más acerca de ambos elementos, el lenguaje de consulta y el entorno de ejecución para dar respuesta a estas consultas. En este tema, por ejemplo, no hemos hablado aún de mutaciones, otra operación importante en GraphQL, ni hemos visto detalles de la consulta, sin embargo, creo que la explicación nos deja un panorama claro de qué es GraphQL.

Ahora sigamos para pronto comenzar a implementar nuestro propio servidor.

### <a name="lesson05"></a> Lección #5 - Servidor HTTP con Express
- - -
Para esta lección, inicializaremos un proyecto de express de la siguiente manera:

```sh
npm init --yes
```

Ahora instalaremos las dependencias de Express y GraphQL con el siguiente comando:

```sh
npm install express graphql
```

Ahora, crearemos un archivo index.js donde pondremos la lógica de nuestro servidor. Una vez que tengamos todo debidamente configurado, ejecutamos el comando

```sh
node index.js
```

Debe decir en la consola "Servidor iniciado" y si vamos a localhost:8080 debe aparecer el mensaje "Hola mundo"

### <a name="lesson06"></a> Lección #6 - Definir el schema
- - -
Creación de un schema de GraphQL mediante objetos. En otro bloque de este curso lo haremos mediante la función *buildSchema*.

### <a name="lesson07"></a> Lección #7 - Hola mundo con GraphQL
- - -
En esta lección, vamos a resolver nuestra consulta de GraphQL.

### <a name="lesson08"></a> Lección #8 - Tipos objeto en el schema
- - -
En esta lección haremos un ejemplo un poco más realista de como funciona GraphQL usando tipos de objeto como GraphQLString, GraphQLInt y GraphQLObjectType.

## <a name="module02"></a> Fundamentos de GraphQL

### <a name="lesson09"></a> Lección #9 - El schema
- - -
Cuando realizamos una consulta a un servidor de GraphQL, el entorno de ejecución valida que los campos que solicitamos existan, que la consulta sea válida, que los objetos que solicitamos tengan indicados los sub campos que requerimos, etc.

Para poder hacer esto, es necesario que definamos qué objetos y campos pueden consultarse de nuestro servicio de GraphQL, sin esta definición, ni el cliente ni el entorno de ejecución podrían validar o indicarnos qué podemos y qué no podemos solicitar de un servicio web de GraphQL.

Sin embargo, recordemos que GraphQL es una forma de crear servicios web y consultarlos que no asume ningún lenguaje de programación, uno puede implementar un servidor de GraphQL en el lenguaje que quiera, Ruby, JavaScript, Python, PHP, etc. Es por esto que para definir los datos que podemos consultar, las estructuras y sus tipos, GraphQL define su propio lenguaje de definción de tipos, el lenguaje de esquema de GraphQL o el GraphQL schema language.

En esta definición encontramos, dos principales características del servicio web de GraphQL:
1. Qué operaciones de consulta y manipulación de datos podemos ejecutar.
2. La estructura de los datos que podemos consultar y el sistema de tipado.

### <a name="lesson10"></a> Lección #10 - Sistema de tipado en GraphQL
- - -
El bloque fundamental de el schema en GraphQL son los tipos. A través del sistema de tipado de GraphQL podemos definir las estructuras de nuestros datos y el tipo de cada propiedad de la que se compone nuestro servicio web.

En general, podemos decir que existen dos elementos importantes en el sistema de tipado, los tipos objetos y los tipos escalares, los primeros definen las estructuras de nuestro servicio web, en CódigoFacilito por ejemplo podríamos tener el tipo objeto Curso, Video, Usuario y en la definición de cada una de estas estructuras colocaríamos las propiedades que las componen, por ejemplo:

```sh
type Course{
  title: String
  id: ID!
  user: User!
}
```

Las estructuras objeto están compuestas por sub campos que pueden ser de dos tipos, otras estructuras objeto para especificar relaciones entre nuestros datos y los tipos escalares.

En este contexto, nos referimos a escalar, o scalar en inglés, para distinguir a las propiedades atómicas, es decir, la unidad indivisible. En GraphQL los tipos escalares no tienen sub campos, según la documentación podemos pensar en ellos como las hojas de nuestro esquema, los datos finales como pueden ser cadenas, números, booleanos, entre otros.

Los tipos escalares vienen predefinidos en GraphQL, aunque podemos definir más, y son estos:

Int: Un entero de 32 bits Float: Un valor decimal con punto flotante. value. String: Una secuencia de caracteres Boolean: Un valor que puede ser verdadero o falso ID: Un identificador único de la estructura

Eventualmente, cualquier estructura que definamos en nuestro esquema, tienen que representarse con alguno de estos tipos escalares, en el caso del ejemplo anterior podemos ver que tenemos los tipos escalares title que es una cadena y id que es un identificador. El tipo User que es objeto, deberá tener sus propios tipos escalares.

### <a name="lesson11"></a> Lección #11 - Los tipo Query y Mutation
- - -
Definir tus estructuras y sus tipos escalares no es suficiente para realizar una consulta a GraphQL. Para iniciar debemos de definir dos tipos importantes en el schema, el tipo Query y el tipo Mutation.

El primero, el tipo Query es donde definimos las operaciones de consulta que se pueden realizar a nuestro servicio web, por ejemplo, si quisiéramos poder consultar todos los cursos en nuestro ejemplo, necesitaríamos definir dicha operación en el tipo Query:

```sh
type Query {
   getCourses: [Course]
}
```

Aquí estoy iniciando la definición del tipo Query, más adelante veremos más detalles prácticos de la definición, por ahora solo es importante indicar que definimos una operación getCourses que retorna un arreglo de objetos con el tipo Course.

Según la documentación de GraphQL, todos los esquemas de GraphQL deben tener un tipo Query, que en otras palabras significa que este tipo es obligatorio en tu esquema.

Si antes has trabajado con servicios web REST, debes de pensar en las operaciones enlistadas en el tipo Query como operaciones tipo GET, de consulta, que no deberían de alterar o manipular la información en el servicio web, solo leerla.

Para operaciones que alteren la información de nuestro servicio web, tenemos el tipo Mutation, donde enlistamos precisamente estas operaciones, las de modificaciones, que pueden ser crear nuevos registros, actualizarlos, reordenarlos o eliminarlos.

Fuera de la separación entre operaciones de consulta y operaciones de modificación, los tipos Mutation y Query son muy similares, por ejemplo, así definiríamos una operación en el tipo mutación:

```sh
type Mutation {
   addCourse(title: String)
}
```

Puedes notar que en el ejemplo de la operación de mutación, la operación además define un argumento que debe recibir, ten en cuenta que también las operaciones de consulta pueden recibir argumentos, veremos más detalles cuando cubramos el tópico de argumentos en el curso.

Por ahora ya tienes una base de qué es el schema y qué debemos definir ahí. A continuación pasemos a la definición de nuestro primer esquema para pasar estos conceptos a la práctica.

## <a name="module03"></a> Mi primer servicio web con GraphQL

### <a name="lesson12"></a> Lección #12 - Preparando el proyecto
- - -
Para esta lección, iniciaremos un nuevo proyecto e instalaremos las siguientes dependencias:

```sh
npm init --yes
npm install express express-graphql graphql
```

Podemos instalar de forma global nodemon para no tener que estar actualizando el servidor por cada modificación que sufra, con el comando:

```sh
npm install -g nodemon
```

Solo nos enfocaremos en hacer funcionar el proyecto con nodemon.

*Nota: También se puede escribir 'npm start' pero solo aplica para el start, no para los demás scripts del package.json*

### <a name="lesson13"></a> Lección #13 - GraphQL Schema Language
- - -
Como vimos en la lección #6, existen dos formas de definir un schema, la primera mediante objetos y la segunda mediante la función buildSchema. En esta lección, vamos a usar buildSchema.

El uso de buildSchema hace que nuestro código en GraphQL sea mucho menos verboso y más fácil de leer.

**Nota:** El uso del signo de exclamación en los tipos de las propiedades significa que es obligatorio.

En el tipo Query es donde definimos nuestras consultas que podemos hacer hacia GraphQL. En este ejemplo, definiremos un método getCourses que lo que hará es devolver un arreglo del tipo Course que definimos anteriormente.

Los métodos que definamos no deben llevar parentesis, salvo que vayamos a pasarle argumentos a la consulta.

### <a name="lesson14"></a> Lección #14 - Mocking de datos
- - -
Haremos mockings de datos de los cursos.

### <a name="lesson15"></a> Lección #15 - Preparando las consultas
- - -
A pesar de que usemos el método buildSchema para plantear nuestro *Schema definition language*, no existe una manera, a diferencia de los objetos, para definir los resolvers. Más adelante tendremos herramientas que nos permitirán definir schemas con lo mejor de los mundos pero es necesario saber que **debemos preferir la notación de objetos que la del buildSchema**.

Para esta lección usaremos **express-graphql** donde una de sus funciones es en ayudarnos a definir los resolvers. Para ello, en la propiedad rootValue debemos pasarle un objeto JSON con todos los resolvers que **tengan el mismo nombre con el que definimos las consultas**, es decir, si una consulta se llama holaMundo, el resolver asignado para dicha consulta también debe llamarse holaMundo().

https://www.npmjs.com/package/express-graphql

### <a name="lesson16"></a> Lección #16 - GraphiQL
- - -
Es una herramienta del navegador que nos permite escribir, validar y testear queries de GraphQL. Podemos accerder a ella gracias al middleware que establecimos con express-graphql (en este caso /graphql).

Vamos a explorar la interfaz. A la derecha tenemos una opción *docs* que es el **Documentation Explorer**. Si nos fijamos, tenemos un tipo Query que al entrar nos muestra todas las consultas que hemos definido. Si entramos en getCourses veremos el tipo Course que definimos lecciones anteriores.

Que quiere decir todo esto, que una de las bondades de graphQL es que nos escribe toda la documentación de nuestro servicio web de forma automática.

*Nota:* Un atajo para autocompletar en GraphQL es presionando CTRL + Espacio. Esto lo sabe GraphQL gracias a nuestro schema que definimos en el proyecto. 

Para este ejemplo definiremos la siguiente consulta:

```sh
query {
  getCourses {
    title
  }
}
```

Pero si le damos click al botón *Prettify*, nos ajustará el código de la siguiente manera:

```sh
{
  getCourses {
    title
  }
}
```

Así es como debemos escribir las consultas habitualmente. La palabra reservada *query* es opcional. Si le damos click al botón de play podremos ver el siguiente resultado:

```sh
{
  "data": {
    "getCourses": [
      {
        "title": "Curso de GraphQL"
      },
      {
        "title": "Curso de JavaScript"
      }
    ]
  }
}
```

En el botón history podemos ver el historial de consultas que hemos ejecutado.

### <a name="lesson17"></a> Lección #17 - Consultar recurso individual
- - -
En esta lección, vamos a crear la consulta que nos servirá para consultar un curso en especifico. La consulta en GraphQL es la siguiente:

```sh
{
  getCourse(id: 1) {
    title
  }
}
```

El resultado sería el siguiente:

```sh
{
  "data": {
    "getCourse": {
      "title": "Curso de GraphQL"
    }
  }
}
```

También podemos hacer dos consultas al mismo tiempo, por ejemplo:

```sh
{
  getCourses {
    id
		title
    views
  }
  getCourse(id: 1) {
    title
  }
}
```

El resultado sería el siguiente:

```sh
{
  "data": {
    "getCourses": [
      {
        "id": "1",
        "title": "Curso de GraphQL",
        "views": 1000
      },
      {
        "id": "2",
        "title": "Curso de JavaScript",
        "views": 50000
      }
    ],
    "getCourse": {
      "title": "Curso de GraphQL"
    }
  }
}
```

*Nota: Estas dos últimas consultas se están ejecutando en paralelo.*

### <a name="lesson18"></a> Lección #18 - Variables de consulta
- - -
Lo podemos ver en la parte inferior izquierda de la pantalla de GraphiQL *QUERY VARIABLES*. Estas, se deben escribir en formato JSON de la siguiente manera:

```sh
{
  "id": "1"
}
```

Y nuestra consulta en GraphQL debe escribirse de la siguiente mnera:

```sh
query ($id: ID!) {
  getCourse(id: $id) {
    title
  }
}
```

Dando como resultado:

```sh
{
  "data": {
    "getCourse": {
      "title": "Curso de GraphQL"
    }
  }
}
```

Ahora solo basta con cambiar las query variables para modificar nuestra consulta.

### <a name="lesson19"></a> Lección #19 - Mutaciones
- - -
Las mutaciones a diferencias de los queries se ejcutan de forma secuencial y tal cual como se indicó en lecciones anteriores, solo debemos usarlas cuando vayamos hacer una modificación, creación o eliminación de un elemento.

*Nota: Cuando declaramos los resolvers lo hacemos con destructuring de objetos de JavaScript.* https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Destructuring_assignment

La consulta en GraphQL es la siguiente:

```sh
mutation {
  addCourse(title: "Curso de Node", views: 10000) {
    id
    title
    views
  }
}
```

**Importante:** se debe escribir la palabra reservada *mutation*, de lo contrario el valor por defecto que escogerá GraphQL será query.

El resultado sería el siguiente:

{
  "data": {
    "addCourse": {
      "id": "3",
      "title": "Curso de Node",
      "views": 10000
    }
  }
}

Ahora si consultamos todos los recursos podemos observar nuestro nuevo registro. Eso significa que nuestra mutación está funcionando de manera correcta.

Para efectos de este proyecto, es muy sencillo porque solamente estamos trabajando con un mocking de datos. Interesante será más adelante cuando modifiquemos una base de datos.

### <a name="lesson20"></a> Lección #20 - Actualizar registros
- - -
En esta lección, crearemos una mutación para actualizar los registros. En GraphQL tendríamos lo siguiente:

```sh
mutation {
  updateCourse(id: "2", title: "Curso de JavaScript V2", views: 10000) {
    id
    title
    views
  }
}
```

Tendríamos el siguiente resultado:

```sh
{
  "data": {
    "updateCourse": {
      "id": "2",
      "title": "Curso de JavaScript V2",
      "views": 10000
    }
  }
}
```

Ahora si consultamos todos los cursos, debemos ver el cambio de forma exitosa.

### <a name="lesson21"></a> Lección #21 - Input types
- - -
En esta lección vamos a hacer uso de los input types. Estos nos permite definir desde un solo lugar del schema los tipos de inputs que recibirán los resolvers (que es muy útil para no replicar código).

Ahora, nuestras mutaciones de GraphQL serían de la siguiente manera:

```sh
mutation {
  addCourse (input: {title: "Curso de Java", views: 100}) {
    title
  }
}
```

```sh
mutation {
  updateCourse(id: 3, input: {title: "Curso de Java V2", views: 100}) {
    title
  }
}
```

### <a name="lesson22"></a> Lección #22 - Eliminar registros
- - -
En esta lección, vamos a eliminar un registro con GraphQL. La consulta sería de la siguiente manera:

```sh
mutation {
  deleteCourse(id: "1") {
    message
  }
}
```

El resultado será el siguiente:

```sh
{
  "data": {
    "deleteCourse": {
      "message": "El curso con el id 1 fue eliminado"
    }
  }
}
```

Si consultamos todos los cursos, podemos comprobar que no tendremos el curso con el id #1.

### <a name="lesson23"></a> Lección #23 - Paginación
- - -
Como último ejercicio del bloque, vamos a realizar una paginación sobre la consulta getCourses. La consulta de GraphQL sería la siguiente:

```sh
{
  getCourses(page: 1, limit: 3) {
    title
  }
}
```

Si modificamos la página o el limite podemos ver distintos resultados de la paginación. Si no queremos ver la paginación, basta con realizar lo siguiente:

```sh
{
  getCourses {
    title
  }
}
```

## <a name="module04"></a> Apollo GraphQL

### <a name="lesson24"></a> Lección #24 - ¿Qué es Apollo GraphQL?
- - -
Apollo GraphQL es la herramienta que nos permite reescribir el ejemplo anterior pero con ciertos beneficios. Antes de hablar de ello es importante saber que nuestro proyecto anterior tienen distintas deficiencias como: Modularización (ya que estamos definiendo todo el schema en un solo archivo) y el uso de resolvers que es limitado cuando usamos la función buildSchema.

Todos estos problemas los vamos a solucionar con [Apollo Server](https://www.apollographql.com/docs/intro/platform/) donde haremos los servicios de una forma más rápida con distintas herramientas que ofrecen.

No hay resolvers personalizados y no hay modularización con el buildSchema pero eso lo resolveremos con Apollo Server.

### <a name="lesson25"></a> Lección #25 - Servidor con ApolloServer
- - -
Para este módulo, crearemos un nuevo proyecto:

```sh
npm init --yes
```

También instalaremos una serie de dependencias que necesitaremos para el proyecto:

```sh
npm install graphql apollo-server graphql-tools
```

El servidor HTTP ahora lo vamos a hacer con Apollo Server pero no con Express. Apollo Server ya está listo para recibir las peticiones y responderlas apropiadamente.

### <a name="lesson26"></a> Lección #26 - Definición de tipos
- - -
En esta lección vamos a definir un schema y esto se compone de dos cosas:
1. La definición de tipos.
2. La definición de resolvers

Esto cuando estamos usando las GraphQL Tools.

Una vez tengamos todo definido, ingresamos a la url que nos provee Apollo Server (en este caso http://localhost:4000/) que es su propia versión de GraphiQL pero con otro CSS. Para la siguiente lección vamos a definir un query para obtener todos los cursos.

### <a name="lesson27"></a> Lección #27 - Queries
- - -
En los resolvers vamos a definir las operaciones que van a dar respuesta tanto a las consultas como a las mutaciones. Para esta lección vamos a definir el resolver para getCourses.

Podemos ver la documentación oficial en https://www.graphql-tools.com/docs/resolvers/

Los métodos que definiremos en estos resolvers son distintos a los del bloque anterior. No nos preocupemos por ahora si no entendemos todos los parámetros que recibe ya que en lecciones posteriores los veremos poco a poco.

### <a name="lesson28"></a> Lección #28 - Mutaciones
- - -
En esta lección, vamos a definir la mutación addCourses. La diferencia de hacerlo con Apollo Server y GraphQL Tools es que se ve claramente la separación entre las consultas y las mutaciones pero las demás ventajas lo veremos en futuras lecciones.

### <a name="lesson29"></a> Lección #29 - Terminando el servidor
- - -
En esta lección vamos a terminar de pasar el resto de consultas y mutaciones que hicimos en el bloque anterior.

## <a name="module05"></a> Base de datos

### <a name="lesson30"></a> Lección #30 - Configurando la base de datos
- - -
En esta lección debemos tener instalado MongoDB. Crearemos un nuevo proyecto con el comando:

```sh
npm init --yes
```

Y debemos tener instalado las siguientes dependencias:

```sh
npm install express graphql-server-express graphql graphql-tools mongoose
```

Solamente nos limitaremos a configurar y dejar funcionando nuestro servidor.

### <a name="lesson31"></a> Lección #31 - Modelo
- - -
Creación del modelo Course con mongoose.

### <a name="lesson32"></a> Lección #32 - Configurar GraphQL con Express
- - -
Ahora vamos a configurar nuestro servidor de GraphQL con nuestro servidor de Express.

Para elminar el mensaje *POST body missing. Did you forget use body-parser middleware?* basta con solo importar el middleware:

```sh
app.use(express.json());
```

De esta forma o como la tenemos en server.js

### <a name="lesson33"></a> Lección #33 - Definición de tipos organizada por archivos
- - -
En esta lección, crearemos el tipo cursos en otro lugar de server.js. El objetivo de este bloque es tener definido nuestros tipos en distintos archivos para modularizar el código.

### <a name="lesson34"></a> Lección #34 - Resolvers modularizados
- - -
Para unir los resolvers, ns apoyaremos de la siguiente dependencia:

```sh
npm install lodash
```

**lodash** nos permite fusionar objetos JSON.

Por ahora no tenemos cursos porque la idea es traer estos registros de la base de datos.

### <a name="lesson35"></a> Lección #35 - Crear registros
- - -
Para esta lección, crearemos registros en la base de datos.

### <a name="lesson36"></a> Lección #36 - Consultar registros
- - -
Para esta lección, consultaremos registros en la base de datos.

### <a name="lesson37"></a> Lección #37 - Actualizar registros
- - -
Para esta lección, editaremos registros en la base de datos.

### <a name="lesson38"></a> Lección #38 - Eliminar registros
- - -
Para esta lección, eliminaremos registros en la base de datos.

### <a name="lesson39"></a> Lección #39 - Paginación
- - -
Para esta lección, implementaremos la paginación mediante la base de datos.

## <a name="module06"></a> Relaciones

### <a name="lesson40"></a> Lección #40 - Nuevo modelo de usuario
- - -
Vamos a hacer una relación donde un usuario puede tener muchos cursos pero un curso le pertenece a un solo usuario.

### <a name="lesson41"></a> Lección #41 - Establecer relaciones
- - -
En esta lección, realizaremos relaciones a nivel de mongoose, modificaremos nuestros resolvers y nuestros types en GraphQL.

En la siguiente lección, veremos como extraer el correo del usuario desde una misma consulta, para ello debemos hacer la relación con GraphQL.

### <a name="lesson42"></a> Lección #42 - Relaciones en GraphQL
- - -
En esta lección, vamos hacer relaciones usando el populate de mongoose y las relaciones de GraphQL. El problema que radica de usar el populate es que sino queremos llamar a esa propiedad no nos traerá e registro. Eso lo corregiremos en la siguiente lección.

### <a name="lesson43"></a> Lección #43 - Resolvers para subcampos
- - -
En esta lección, dejaremos de usar el método populate de mongoose y escribiremos resolvers personalizados para subcampos de nuestros **tipos objeto**.

## <a name="module07"></a> Autenticación

### <a name="lesson44"></a> Lección #44 - Encriptar contraseña
- - -
En esta lección, nos enfocaremos en encriptar la contraseña. Para ello, debemos instalar la dependencia: 

```sh
npm install bcrypt
```

### <a name="lesson45"></a> Lección #45 - Iniciar sesión
- - -
En esta lección realizaremos el método de inicio de sesión.

### <a name="lesson46"></a> Lección #46 - Generando JSON Web Token
- - -
En esta lección, generaremos el token con JSON Web Token, para ello lo instalamos con la dependencia:

```sh
npm install jsonwebtoken
```

### <a name="lesson47"></a> Lección #47 - Apollo Server v2
- - -
Antes de que pasemos a la integración del módulo de autenticación con GraphQL, primero debemos hacer unos cambios muy importantes. La dependencia graphql-server-express cambió a apollo-server-express y lo instalamos de la siguiente manera:

```sh
npm install apollo-server-express
```

El método makeExecutableSchema (mejor dicho, graphql-tools ya viene integrado en ese paquete) ya viene integrado en este paquete.

El endpoint es el siguiente http://localhost:8080/graphql

Lo bueno de esto es que nos mantenemos actualizados y podemos contar con los HTTP Headers en el entorno gráfico.

### <a name="lesson48"></a> Lección #48 - Contexto GraphQL
- - -
En la [documentación de GraphQL](https://www.apollographql.com/docs/apollo-server/security/authentication/) nos indica que la autenticación lo vamos a realizar con el contexto. El valor que retornemos de él se convertirá en nuestro tercer parámetro de los resolvers de GraphQL, eso significa que los tipos que están en el nivel superior van a recibir como primer argumento en su resolver lo que hayamos retornado del contexto (en este caso, el currentUser y el token).

Los HTTP Headers en GraphQL se pasan de la siguiente manera:

```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlZThkNTI5ZjIyZTZhODZjNGIyMjEyZSIsImlhdCI6MTU5MjMxNzIzMX0.mTmmaapNeV9NsAG1ZVI0q6uD2c1ynCSc78qDDXso6IE"
}
```

### <a name="lesson49"></a> Lección #49 - ¿Cómo proteger mis endpoints?
- - -
Vamos a proteger el resolver addCourse gracias al contexto creado en la lección anterior, la única manera de crear cursos es estando authenticado en la aplicación. Para más consejos sobre este apartado lo mejor es consultar la documentación indicada en la lección anterior.